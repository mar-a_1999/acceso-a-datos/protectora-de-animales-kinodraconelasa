﻿/* creación de la base de datos */

DROP DATABASE IF EXISTS protectora_animales;
CREATE DATABASE protectora_animales;

USE protectora_animales;

-- crear las tablas 

  -- local

  CREATE OR REPLACE TABLE local(
    codigo_local int AUTO_INCREMENT, -- cuando un campo se tipifica como incremental SIEMPRE va a ser la clave principal
    direccion varchar(40),
    numero_empleados int,
    numero_animales int,
    PRIMARY KEY(codigo_local)
  );

-- empleado

  CREATE OR REPLACE TABLE empleado(
    codigo_empleado int AUTO_INCREMENT,
    codigo_local int,
    nombre varchar(30),
    apellidos varchar(40),
    edad int,
    dni varchar(10),
    PRIMARY KEY(codigo_empleado)
  );

-- animal

  CREATE OR REPLACE TABLE animal(
    codigo_animal int AUTO_INCREMENT,
    codigo_local int,
    nombre varchar(30),
    raza varchar(30),
    esta_vacunado boolean,
    PRIMARY KEY(codigo_animal)
  );

-- cliente

  CREATE OR REPLACE TABLE cliente(
    codigo_cliente int AUTO_INCREMENT, -- cuando un campo se tipifica como incremental SIEMPRE va a ser la clave principal
    direccion varchar(40),
    dni varchar(10),
    PRIMARY KEY(codigo_cliente)
  );

-- socio

  CREATE OR REPLACE TABLE socio(
   codigo_socio int AUTO_INCREMENT,
   codigo_empleado int,
   codigo_local int,
   nombre varchar(30),
   dni varchar(10),
   direccion varchar(40),
   PRIMARY KEY(codigo_socio)
  );

  -- telefono_cliente

 CREATE OR REPLACE TABLE telefono_cliente(
  id_telefono_cliente int AUTO_INCREMENT,
  codigo_cliente int,
  telefono varchar(9),
  PRIMARY KEY(id_telefono_cliente)
  );

  -- telefono_socio

 CREATE OR REPLACE TABLE telefono_socio(
  id_telefono_socio int AUTO_INCREMENT,
  codigo_socio int,
  telefono varchar(9),
  PRIMARY KEY(id_telefono_socio)
  );

  -- adopta_cliente

 CREATE OR REPLACE TABLE adopta_cliente(
  id_adopta_cliente int AUTO_INCREMENT,
  codigo_animal int,
  codigo_cliente int,
  PRIMARY KEY(id_adopta_cliente)
  );

  -- adopta_socio

 CREATE OR REPLACE TABLE adopta_socio(
  id_adopta_socio int AUTO_INCREMENT,
  codigo_animal int,
  codigo_cliente int,
  PRIMARY KEY(id_adopta_socio)
  );

  -- atiende_cliente

 CREATE OR REPLACE TABLE atiende_cliente(
  id_atiende_cliente int AUTO_INCREMENT,
  codigo_cliente int,
  codigo_empleado int,
  PRIMARY KEY(id_atiende_cliente)
  );

 -- creacion de las restricciones 

 -- empleado

ALTER TABLE empleado
  ADD CONSTRAINT fk_local_empleado
  FOREIGN KEY (codigo_local)
  REFERENCES local(codigo_local);

 -- animal

ALTER TABLE animal
  ADD CONSTRAINT fk_local_animal
  FOREIGN KEY (codigo_local)
  REFERENCES local(codigo_local);

 -- empleado

ALTER TABLE socio
  ADD CONSTRAINT fk_empleado_socio
  FOREIGN KEY (codigo_empleado)
  REFERENCES empleado(codigo_empleado),
  ADD CONSTRAINT fk_local_socio
  FOREIGN KEY (codigo_local)
  REFERENCES local(codigo_local),
  ADD CONSTRAINT uk_codigo_empleado_socio
  UNIQUE KEY (codigo_empleado);


  -- telefono_cliente

ALTER TABLE telefono_cliente
  ADD CONSTRAINT fk_telefono_cliente
  FOREIGN KEY (codigo_cliente)
  REFERENCES cliente(codigo_cliente),
  ADD CONSTRAINT uk_telefono_cliente_cliente_telefono
  UNIQUE KEY (codigo_cliente,telefono);
  
  -- telefono_socio

ALTER TABLE telefono_socio
  ADD CONSTRAINT fk_telefono_socio
  FOREIGN KEY (codigo_socio)
  REFERENCES socio(codigo_socio),
  ADD CONSTRAINT uk_telefono_socio_socio_telefono
  UNIQUE KEY (codigo_socio,telefono);

  -- adopta_cliente

ALTER TABLE adopta_cliente
  ADD CONSTRAINT fk_adopta_cliente
  FOREIGN KEY (codigo_animal)
  REFERENCES animal(codigo_animal),
  ADD CONSTRAINT fk_adopta_cliente2
  FOREIGN KEY (codigo_cliente)
  REFERENCES cliente(codigo_cliente),
  ADD CONSTRAINT uk_adopta_cliente_cliente_adopta
  UNIQUE KEY (codigo_animal,codigo_cliente);

  -- adopta_socio
    
ALTER TABLE adopta_socio
  ADD CONSTRAINT fk_adopta_socio
  FOREIGN KEY (codigo_animal)
  REFERENCES animal(codigo_animal),
  ADD CONSTRAINT fk_adopta_socio2
  FOREIGN KEY (codigo_socio)
  REFERENCES socio(codigo_socio),
  ADD CONSTRAINT uk_adopta_socio_socio_adopta
  UNIQUE KEY (codigo_animal,codigo_socio);

   -- atiende_cliente

ALTER TABLE atiende_cliente
  ADD CONSTRAINT fk_atiende_cliente
  FOREIGN KEY (codigo_cliente)
  REFERENCES cliente(codigo_cliente),
  ADD CONSTRAINT fk_atiende_cliente2
  FOREIGN KEY (codigo_empleado)
  REFERENCES empleado(codigo_empleado),
  ADD CONSTRAINT uk_atiende_cliente_cliente_atiende
  UNIQUE KEY (codigo_cliente,codigo_empleado);
       