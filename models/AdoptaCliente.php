<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "Adopta_cliente".
 *
 * @property int $id_adopta_cliente
 * @property int|null $codigo_animal
 * @property int|null $codigo_cliente
 */
class AdoptaCliente extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'Adopta_cliente';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo_animal', 'codigo_cliente'], 'integer'],
            [['codigo_animal', 'codigo_cliente'], 'unique', 'targetAttribute' => ['codigo_animal', 'codigo_cliente']],
            [['codigo_animal'], 'exist', 'skipOnError' => true, 'targetClass' => Animal::className(), 'targetAttribute' => ['codigo_animal' => 'codigo_animal']],
            [['codigo_cliente'], 'exist', 'skipOnError' => true, 'targetClass' => Cliente::className(), 'targetAttribute' => ['codigo_cliente' => 'codigo_cliente']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_adopta_cliente' => 'Id Adopta Cliente',
            'codigo_animal' => 'Codigo Animal',
            'codigo_cliente' => 'Codigo Cliente',
        ];
    }
}
