<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "Adopta_socio".
 *
 * @property int $id_adopta_socio
 * @property int|null $codigo_animal
 * @property int|null $codigo_socio
 */
class AdoptaSocio extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'Adopta_socio';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo_animal', 'codigo_socio'], 'integer'],
            [['codigo_animal', 'codigo_socio'], 'unique', 'targetAttribute' => ['codigo_animal', 'codigo_socio']],
            [['codigo_animal'], 'exist', 'skipOnError' => true, 'targetClass' => Animal::className(), 'targetAttribute' => ['codigo_animal' => 'codigo_animal']],
            [['codigo_socio'], 'exist', 'skipOnError' => true, 'targetClass' => Socio::className(), 'targetAttribute' => ['codigo_socio' => 'codigo_socio']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_adopta_socio' => 'Id Adopta Socio',
            'codigo_animal' => 'Codigo Animal',
            'codigo_socio' => 'Codigo Socio',
        ];
    }
}
