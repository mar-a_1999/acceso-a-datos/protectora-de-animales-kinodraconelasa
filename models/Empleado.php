<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "Empleado".
 *
 * @property int $codigo_empleado
 * @property int|null $codigo_local
 * @property string|null $nombre
 * @property string|null $apellidos
 * @property int|null $edad
 * @property string|null $dni
 */
class Empleado extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'Empleado';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo_local', 'edad'], 'integer'],
            [['nombre'], 'string', 'max' => 30],
            [['apellidos'], 'string', 'max' => 40],
            [['dni'], 'string', 'max' => 10],
            [['codigo_local'], 'exist', 'skipOnError' => true, 'targetClass' => Local::className(), 'targetAttribute' => ['codigo_local' => 'codigo_local']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo_empleado' => 'Codigo Empleado',
            'codigo_local' => 'Codigo Local',
            'nombre' => 'Nombre',
            'apellidos' => 'Apellidos',
            'edad' => 'Edad',
            'dni' => 'Dni',
        ];
    }
}
