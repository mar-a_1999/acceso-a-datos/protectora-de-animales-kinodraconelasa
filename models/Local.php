<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "Local".
 *
 * @property int $codigo_local
 * @property string|null $direccion
 * @property int|null $numero_empleados
 * @property int|null $numero_animales
 */
class Local extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'Local';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['numero_empleados', 'numero_animales'], 'integer'],
            [['direccion'], 'string', 'max' => 40],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo_local' => 'Codigo Local',
            'direccion' => 'Direccion',
            'numero_empleados' => 'Numero Empleados',
            'numero_animales' => 'Numero Animales',
        ];
    }
}
