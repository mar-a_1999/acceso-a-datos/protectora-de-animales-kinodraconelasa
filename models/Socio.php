<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "Socio".
 *
 * @property int $codigo_socio
 * @property int|null $codigo_empleado
 * @property int|null $codigo_local
 * @property string|null $nombre
 * @property string|null $dni
 * @property string|null $direccion
 */
class Socio extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'Socio';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo_empleado', 'codigo_local'], 'integer'],
            [['nombre'], 'string', 'max' => 30],
            [['dni'], 'string', 'max' => 10],
            [['direccion'], 'string', 'max' => 40],
            [['codigo_empleado'], 'unique'],
            [['codigo_empleado'], 'exist', 'skipOnError' => true, 'targetClass' => Empleado::className(), 'targetAttribute' => ['codigo_empleado' => 'codigo_empleado']],
            [['codigo_local'], 'exist', 'skipOnError' => true, 'targetClass' => Local::className(), 'targetAttribute' => ['codigo_local' => 'codigo_local']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo_socio' => 'Codigo Socio',
            'codigo_empleado' => 'Codigo Empleado',
            'codigo_local' => 'Codigo Local',
            'nombre' => 'Nombre',
            'dni' => 'Dni',
            'direccion' => 'Direccion',
        ];
    }
}
