<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "Telefono_socio".
 *
 * @property int $id_telefono_socio
 * @property int|null $codigo_socio
 * @property string|null $telefono
 */
class TelefonoSocio extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'Telefono_socio';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo_socio'], 'integer'],
            [['telefono'], 'string', 'max' => 9],
            [['codigo_socio', 'telefono'], 'unique', 'targetAttribute' => ['codigo_socio', 'telefono']],
            [['codigo_socio'], 'exist', 'skipOnError' => true, 'targetClass' => Socio::className(), 'targetAttribute' => ['codigo_socio' => 'codigo_socio']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_telefono_socio' => 'Id Telefono Socio',
            'codigo_socio' => 'Codigo Socio',
            'telefono' => 'Telefono',
        ];
    }
}
