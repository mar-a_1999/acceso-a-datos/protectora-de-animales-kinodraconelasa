<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\AdoptaCliente */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="adopta-cliente-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'codigo_animal')->textInput() ?>

    <?= $form->field($model, 'codigo_cliente')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
