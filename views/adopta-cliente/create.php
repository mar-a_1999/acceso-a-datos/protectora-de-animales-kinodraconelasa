<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\AdoptaCliente */

$this->title = 'Create Adopta Cliente';
$this->params['breadcrumbs'][] = ['label' => 'Adopta Clientes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="adopta-cliente-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
