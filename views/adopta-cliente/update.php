<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\AdoptaCliente */

$this->title = 'Update Adopta Cliente: ' . $model->id_adopta_cliente;
$this->params['breadcrumbs'][] = ['label' => 'Adopta Clientes', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_adopta_cliente, 'url' => ['view', 'id' => $model->id_adopta_cliente]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="adopta-cliente-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
