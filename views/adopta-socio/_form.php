<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\AdoptaSocio */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="adopta-socio-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'codigo_animal')->textInput() ?>

    <?= $form->field($model, 'codigo_socio')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
