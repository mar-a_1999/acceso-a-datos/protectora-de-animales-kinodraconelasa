<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Adopta Socios';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="adopta-socio-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Adopta Socio', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id_adopta_socio',
            'codigo_animal',
            'codigo_socio',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
