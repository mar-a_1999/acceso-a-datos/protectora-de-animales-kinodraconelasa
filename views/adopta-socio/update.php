<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\AdoptaSocio */

$this->title = 'Update Adopta Socio: ' . $model->id_adopta_socio;
$this->params['breadcrumbs'][] = ['label' => 'Adopta Socios', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_adopta_socio, 'url' => ['view', 'id' => $model->id_adopta_socio]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="adopta-socio-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
