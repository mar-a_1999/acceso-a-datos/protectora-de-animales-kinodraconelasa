<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\TelefonoCliente */

$this->title = 'Update Telefono Cliente: ' . $model->id_telefono_cliente;
$this->params['breadcrumbs'][] = ['label' => 'Telefono Clientes', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_telefono_cliente, 'url' => ['view', 'id' => $model->id_telefono_cliente]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="telefono-cliente-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
