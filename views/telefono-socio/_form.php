<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\TelefonoSocio */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="telefono-socio-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'codigo_socio')->textInput() ?>

    <?= $form->field($model, 'telefono')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
