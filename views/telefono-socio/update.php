<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\TelefonoSocio */

$this->title = 'Update Telefono Socio: ' . $model->id_telefono_socio;
$this->params['breadcrumbs'][] = ['label' => 'Telefono Socios', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_telefono_socio, 'url' => ['view', 'id' => $model->id_telefono_socio]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="telefono-socio-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
